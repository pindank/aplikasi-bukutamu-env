# Menggunakan Vault di Kubernetes #

Beberapa metode mengakses Vault dalam k8s :

1. Langsung diakses sebagai external service
2. Menggunakan sidecar agent
3. Sebagai ExternalSecret

## Setup Vault ##

1. Install vault

    ```
    helm repo add hashicorp https://helm.releases.hashicorp.com
    helm install vault hashicorp/vault
    ```

2. Inisialisasi vault

    ```
    kubectl exec vault-0 -- vault operator init -key-shares=1 -key-threshold=1
    ```

    Outputnya seperti ini:

    ```
    Unseal Key 1: dhzqFTi6Tq85nPb1VTtXR9Ru7Aso+W6ePKs0Kl6Xu1c=
    Initial Root Token: hvs.hO7Ps2szO5QTEHrl1B03PpIk
    ```

3. Unseal vault

    ```
    VAULT_UNSEAL_KEY=dhzqFTi6Tq85nPb1VTtXR9Ru7Aso+W6ePKs0Kl6Xu1c=  
    kubectl exec vault-0 -- vault operator unseal $VAULT_UNSEAL_KEY
    ```

4. Login ke vault

    ```
    kubectl exec --stdin=true --tty=true vault-0 -- /bin/sh
    ```

5. Enable engine KV

    ```
    vault secrets enable kv-v2
    ```

6. Melihat daftar engine yang aktif

    ```
    vault secrets list
    ```

7. Isi data password database

    ```
    vault kv put secret/bukutamu/db/dev dbpassword="abcd1234"
    ```

8. Cek hasilnya 

    ```
    vault kv get secret/bukutamu/db/dev
    ```

9. Enable kubernetes authentication untuk aplikasi dalam pod supaya bisa login ke vault

    ```
    vault auth enable kubernetes
    ```

10. Aplikasi yang mau connect dicek apakah berada di cluster yang sama dengan Vault

    ```
    vault write auth/kubernetes/config kubernetes_host="https://$KUBERNETES_PORT_443_TCP_ADDR:443"
    ```

11. Buat policy(permission) agar aplikasi bukutamu bisa membaca secret bukutamu

    ```
    vault policy write bukutamu-app-policy-dev - <<EOF
      path "secret/data/bukutamu/db/dev" {
        capabilities = ["read"]
    }
    EOF
    ```

12. Buat role untuk aplikasi bukutamu

    ```
    vault write auth/kubernetes/role/bukutamu-app-role-dev \
    bound_service_account_names=bukutamu-app-sa-dev \
    bound_service_account_namespaces=default \
    policies=bukutamu-app-policy-dev \
    ttl=24h
    ```

13. Buat service account untuk aplikasi bukutamu

    ```
    kubectl create sa bukutamu-app-sa-dev
    ```

14. Cek service account

    ```
    kubectl get serviceaccounts
    ```

## Mengakses isi Vault dengan Agent Sidecar ##

1. Pasang service account di deployment descriptor aplikasi

    ```
    spec:
      serviceAccountName: bukutamu-app-sa-dev
      containers:
      - image: endymuhardin/aplikasi-bukutamu:1.0.1-RELEASE
        name: aplikasi-bukutamu
    ```

2. Tambahkan annotation di deployment descriptor aplikasi

    ```
    spec:
      template:
        metadata:
          annotations:
            vault.hashicorp.com/agent-inject: 'true'
            vault.hashicorp.com/role: 'bukutamu-app-role-dev'
            vault.hashicorp.com/agent-inject-secret-application.properties: 'secret/bukutamu/db/dev'
            vault.hashicorp.com/agent-inject-template-application.properties: |
          {{- with secret "secret/bukutamu/db/dev" -}}
          export SPRING_DATASOURCE_PASSWORD={{ .Data.database.password }}
          {{- end }}
    ```

3. Cek kondisi container agent (bila gagal start)

    ```
    kubectl logs \
    $(kubectl get pod -l app=bukutamu-app-deployment-dev -o jsonpath="{.items[0].metadata.name}") \
     --container vault-agent-init
    ```

4. Menampilkan isi `application.properties` di dalam folder `/vault/secrets/`

    ```
    kubectl exec \
    $(kubectl get pod -l app=bukutamu-app-deployment-dev -o jsonpath="{.items[0].metadata.name}") \
    --container aplikasi-bukutamu -- cat /vault/secrets/application.properties
    ```

5. Tambahkan `/vault/secrets/` ke dalam `SPRING_CONFIG_LOCATION`

    ```
    spec:
      serviceAccountName: bukutamu-app-sa-dev
      containers:
      - image: endymuhardin/aplikasi-bukutamu:1.0.1-RELEASE
        name: aplikasi-bukutamu
      env:
      - name: SPRING_CONFIG_LOCATION
        value: 'classpath:/,/vault/secrets/'
    ```


## Sebagai ExternalSecret ##

1. Instalasi dulu plugin `external-secrets`

    ```
    helm repo add external-secrets https://charts.external-secrets.io
    helm install external-secrets external-secrets/external-secrets
    ```

2. Buat konfigurasi SecretStore

    ```yml
    apiVersion: external-secrets.io/v1beta1
    kind: SecretStore
    metadata:
      name: vault-bukutamu
    spec:
      provider:
        vault:
          server: "http://vault:8200"
          path: "secret"
          version: "v2"
          auth:
            kubernetes:
              mountPath: "kubernetes"
              role: "bukutamu-app-role-dev"
              serviceAccountRef:
                name: "bukutamu-app-sa-dev"
    ```

3. Buat konfigurasi external services

    ```yml
    apiVersion: external-secrets.io/v1beta1
    kind: ExternalSecret
    metadata:
    name: vault-bukutamu-dev
    spec:
      secretStoreRef:
        name: vault-bukutamu
        kind: SecretStore
      target:
        name: bukutamu-db-dev-password
      data:
        - secretKey: dbdev
          remoteRef:
            key: secret/bukutamu/db/dev
            property: dbpassword
    ```

3. Cek isinya

    ```
    kubectl get secrets bukutamu-db-dev-password -o jsonpath='{.data.dbdev}' | base64 -d
    ```

4. Gunakan di deployment

    ```yml
    spec:
      containers:
      - image: postgres:14
        name: aplikasi-bukutamu-db
        env:
          - name: POSTGRES_PASSWORD
            valueFrom:
              secretKeyRef:
                name: bukutamu-db-dev-password
                key: dbdev