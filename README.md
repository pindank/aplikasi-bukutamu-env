# Konfigurasi Deployment untuk Aplikasi Bukutamu #

1. Generate deployment config

    ```
    kubectl create deployment bukutamu-app-deployment-dev --image=endymuhardin/aplikasi-bukutamu:latest --dry-run=client -o=yaml > bukutamu-app-deployment-dev.yaml
    ```

2. Generate service config

    ```
    kubectl create service loadbalancer demo --tcp=8080:8080 --dry-run=client -o=yaml > bukutamu-app-service-dev.yaml
    ```

3. Generate config map

    ```
    kubectl create configmap bukutamu-config-dev --from-literal=spring.datasource.url=jdbc:postgresql://bukutamu-db-dev/bukutamudb
    ```

4. Generate secret

    ```
    kubectl create secret generic bukutamu-secret-dev --from-literal=spring.datasource.password='Cwv3Ley7B34V$E' --dry-run=client -o=yaml 
    ```